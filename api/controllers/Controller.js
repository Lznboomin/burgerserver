'use strict';
var mongoose = require('mongoose'),
Burger = mongoose.model('Burgers');

exports.list_all_burgers = function(req, res) {
    Burger.find({}, function(err, burger) {
        if (err)
        res.send(err);
        res.json(burger)
    });
};

exports.create_a_burger = function(req, res) {
    var new_burger = new Burger(req.body);
    new_burger.save(function(err, burger) {
        if (err)
        res.send(err);
        res.json(burger);
    });
};

exports.list_a_burger = function(req, res) {
    Burger.findById(req.params.id, function(err, burger) {
        if (err)
        res.send(err);
        res.json(burger);
    });
};

exports.update_a_burger = function(req, res) {
    Burger.findOneAndUpdate({_id: req.params.id}, req.body, {new: true}, function(err, burger) {
      if (err)
        res.send(err);
      res.json(burger);
    });
  };

exports.delete_a_burger = function(req, res) {
    Burger.remove({
        _id: req.params.id
    }, function(err, burger) {
        if (err)
        res.send(err);
        res.json({ message: 'Burger edukalt kustutatud' });
    });
};
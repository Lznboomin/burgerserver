'use strict';
module.exports = function(app) {
    var x = require('../controllers/Controller');

    //Routes
    app.route('/burgers')
    .get(x.list_all_burgers)
    .post(x.create_a_burger);

    app.route('/burgers/:id')
    .get(x.list_a_burger)
    .patch(x.update_a_burger)
    .delete(x.delete_a_burger);
};
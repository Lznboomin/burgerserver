'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var BurgerSchema = new Schema({
    name: {type: String, required: true},
    price: {type: Number, required: true},
    rating: {type: Number, required: true},
    location: {type: String, required: true}
});

module.exports = mongoose.model('Burgers', BurgerSchema);
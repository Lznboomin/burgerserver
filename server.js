var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000,
    mongoose = require('mongoose'),
    Burger = require('./api/models/Model'),
    bodyParser = require('body-parser');

app.use(express.static(__dirname + '/'));

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/burgerdb');

app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());

var routes = require('./api/routes/Routes');
routes(app);

app.listen(port);

console.log('Serving on: ' + port);

app.use(function(req, res) {
    res.status(404).send({url: req.originalUrl + ' not found,' + ' try / instead'})
  });